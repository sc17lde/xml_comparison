//IO IMPORT
import java.io.*;

//GUI IMPORTS
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

//SAX IMPORTS
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

//DOM IMPORTS
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Element;
import org.w3c.dom.ls.*;


class project_gui {

 private static JLabel lblFile1;
 private static JLabel lblFile2;
 private static JLabel lblInfo;
 private static JLabel lblResult;
 private static JTextArea txtFile1Preview;
 private static JTextArea txtFile2Preview;
 private static JTextArea txtResults;

 public static void main(String args[]) {

  //Setting up GUI elements
  JFrame frameMain = new JFrame("XML comparator");
  frameMain.setSize(500, 300);
  frameMain.setExtendedState(JFrame.MAXIMIZED_BOTH);

  JPanel panelMain = new JPanel(new GridBagLayout());
  JPanel panelResults = new JPanel(new GridBagLayout());

  final GridBagConstraints gridBagSettings = new GridBagConstraints();
  gridBagSettings.fill = GridBagConstraints.HORIZONTAL;
  gridBagSettings.weightx = 1d;
  gridBagSettings.weighty = 1d;

  final JFileChooser fileSelector = new JFileChooser();

  JButton bntFile1 = new JButton("Select File 1");

  lblFile1 = new JLabel("File 1 not selected", SwingConstants.CENTER);

  lblResult = new JLabel("A problem may have occured, results display here",
   SwingConstants.CENTER);

  JButton bntFile2 = new JButton("Select File 2");
  lblFile2 = new JLabel("File 2 not selected", SwingConstants.CENTER);

  JButton bntCompare = new JButton("Compare selected files");
  bntCompare.setPreferredSize(new Dimension(50, 70));


  frameMain.add(panelMain);
  txtFile1Preview = new JTextArea(1, 1);
  txtFile2Preview = new JTextArea(1, 1);
  txtResults = new JTextArea(1, 1);

  JFrame frameResults = new JFrame("Comparison Results");
  frameResults.setSize(500, 300);

  JScrollPane scrollResults = new JScrollPane(txtResults,
   JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
  scrollResults.setPreferredSize(new Dimension(50, 630));
  gridBagSettings.gridy = 0;
  panelResults.add(scrollResults, gridBagSettings);
  gridBagSettings.gridy = 1;
  panelResults.add(lblResult, gridBagSettings);
  frameResults.add(panelResults);
  frameResults.setExtendedState(JFrame.MAXIMIZED_BOTH);

  JScrollPane scrollFile1Preview = new JScrollPane(txtFile1Preview,
   JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
  scrollFile1Preview.setPreferredSize(new Dimension(50, 210));

  JScrollPane scrollFile2Preview = new JScrollPane(txtFile2Preview,
   JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
  scrollFile2Preview.setPreferredSize(new Dimension(50, 210));

  lblInfo = new JLabel("File Previews limited to 100 lines to conserve memory ",
                                                        SwingConstants.CENTER);

  //Adds elements to panel, then panel to frame
  gridBagSettings.gridy = 0;
  panelMain.add(bntFile1, gridBagSettings);
  gridBagSettings.gridy = 1;
  panelMain.add(lblFile1, gridBagSettings);
  gridBagSettings.gridy = 2;
  panelMain.add(scrollFile2Preview, gridBagSettings);
  gridBagSettings.gridy = 3;
  panelMain.add(bntFile2, gridBagSettings);
  gridBagSettings.gridy = 4;
  panelMain.add(lblFile2, gridBagSettings);
  gridBagSettings.gridy = 5;
  panelMain.add(scrollFile1Preview, gridBagSettings);
  gridBagSettings.gridy = 6;
  panelMain.add(bntCompare, gridBagSettings);
  gridBagSettings.gridy = 7;
  panelMain.add(lblInfo, gridBagSettings);
  frameMain.setVisible(true);

  //File 2 selection button event handler
  bntFile2.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent e) {

    txtFile1Preview.setText("");

    //Asks for file
    int returnVal = fileSelector.showOpenDialog(bntFile2);

    //Stores file in label for future use
    lblFile2.setText(fileSelector.getSelectedFile().getAbsolutePath());

    try {

     //Reads the file
     String fileName = fileSelector.getSelectedFile().getAbsolutePath();
     File file = new File(fileName);
     FileReader fr = new FileReader(file);
     BufferedReader br = new BufferedReader(fr);
     String line;

     //Adds the previw to the textbox
     while((line=br.readLine())!=null&&txtFile1Preview.getLineCount()<100) {
      txtFile1Preview.append(line);
      txtFile1Preview.append("\n");
     }
    } catch (IOException ee) {
     System.out.println("Completed!");
    }

   }
  });

  //Same as above
  bntFile1.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent e) {
    txtFile2Preview.setText("");
    int returnVal = fileSelector.showOpenDialog(bntFile1);
    lblFile1.setText(fileSelector.getSelectedFile().getAbsolutePath());
    try {
     String fileName = fileSelector.getSelectedFile().getAbsolutePath();
     File file = new File(fileName);
     FileReader fr = new FileReader(file);
     BufferedReader br = new BufferedReader(fr);
     String line;
     while((line=br.readLine())!=null&&txtFile2Preview.getLineCount()<100) {
      txtFile2Preview.append(line);
      txtFile2Preview.append("\n");
     }
    } catch (IOException ee) {
     System.out.println("Completed!");
    }

   }
  });

  //Comparison button event handler
  bntCompare.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent e) {

    //Checks the files are given
    if (lblFile1.getText() == "File 1 not selected") {
     JOptionPane.showMessageDialog(null, "File 1 not selected");
    } else if (lblFile2.getText() == "File 2 not selected") {
     JOptionPane.showMessageDialog(null, "File 2 not selected");
    } else {

     try {

      //Reads file 2 & uses a DOM parser
      File inputFile2 = new File(lblFile2.getText());
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(inputFile2);
      doc.getDocumentElement().normalize();

      //Stores all of file 2 as a Node List
      NodeList nList = doc.getElementsByTagName("*");

      //Sets up the SAX parser to read file 1
      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();
      txtResults.setText("");
      txtResults.append("\n-------- ITEMS THAT DON'T APPEAR IN FILE 2 --------\n");

      DefaultHandler handler = new DefaultHandler() {

       int attempts = 0;
       int passed = 0;
       int failed = 0;
       int progress = 0;
       String qname;
       Attributes qattributes;
       boolean HasAMatchingElementBeenFound = false;

       //Code that runs every time SAX reads a new element from file 1
       public void startElement(String uri, String localName, String qName,
        Attributes attributes) throws SAXException {

        HasAMatchingElementBeenFound = false;

        //Outputs a progress indicator to the console
        //Example data usually needs between 70,000 and 90,000 progress
        //This takes about 8 minutes
        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.print("Calculating: " + progress + "\n");

        qname = qName;
        qattributes = attributes;

        //Compares incoming SAX element to stored DOM element
        for (int temp = 0; temp < nList.getLength(); temp++) {
         Node nNode = nList.item(temp);
         if (HasAMatchingElementBeenFound == false) {
          if (nNode.getNodeName().equals(qname)) {
           HasAMatchingElementBeenFound = false;
           int matchcounter = 0;

           int qlen = qattributes.getLength();
           boolean nope = false;

           NamedNodeMap attributes2 = nNode.getAttributes();
           int len = attributes2.getLength();


           int subattempts = 0;
           for (int x = 0; x < len; x++) {
            if ((len != qlen)) {
             break;
            }

            subattempts = subattempts + 1;

            //Compares Element attributes
            for (int y = 0; y < qlen; y++) {

             Attr attr = (Attr) attributes2.item(x);
             String temp1 = attr.getNodeValue();
             String temp2 = qattributes.getValue(y).toString();

             //Counts elements that have a match
             if (temp1.equals(temp2)) {
              matchcounter = matchcounter + 1;
              break;
             }
            }

            //If any of the getAttributes don't match it moves to the next
            if (subattempts != matchcounter) {
             break;
            }
           }

           //If all the attributes match, the element is the same
           if (matchcounter == len) {
            HasAMatchingElementBeenFound = true;
            passed = passed + 1;

            try{
              if(nNode.getChildNodes().getLength() == 0){
                nNode.getParentNode().removeChild(nNode);
              }
            } catch(Exception e) {
              //Ignore
            }

            break;
           }
          }
         }
        }

        //If it gets to the end of the DOM without a match, the element
        //is reported missing in the results box
        if (HasAMatchingElementBeenFound == false) {
         String output = "";

         failed = failed + 1;

         output = output + "<" + qname;

         //As SAX can't return raw XML the element is reformatted to XML
         for (int x = 0; x < qattributes.getLength(); x++) {
          output = output + " " + qattributes.getLocalName(x) + "=\"" +
                                              qattributes.getValue(x) + "\"";
         }
         output = output + ">\n";
         txtResults.append(output);
        }
        attempts = attempts + 1;
       }

       //Executes everytime an element ends
       public void endElement(String uri, String localName,
        String qName) throws SAXException {

        //Currently only used as a progress ticker
        progress = progress + 1;
       }

       //Executes once the SAX file is empty
       public void endDocument() throws SAXException {
        txtResults.append("\n--------  NEW ITEMS THAT APPEAR IN FILE 2  --------\n\n");
        txtResults.append("# The Following attributes are listed in alphabetical order\n");
        txtResults.append("# as the secondary parser is unable to preserve the initial order\n\n\n");

        //If it gets to the end of the DOM without a match, the element
        //is reported missing in the results box
        for (int temp = 10; temp < nList.getLength(); temp++) {


          Node nNode = nList.item(temp);

          if(nNode.getChildNodes().getLength() !=0){
            continue;
          }
          String output = "";

          output = output + "<" + nNode.getNodeName();

          //DOM can export XML with help, but ti won't match the SAX XML
          //For ease of comparison, it is also reformatted to XML
          NamedNodeMap attributes2 = nNode.getAttributes();
          for (int x = 0; x < attributes2.getLength(); x++) {
           Attr attr = (Attr) attributes2.item(x);
           output = output + " " + attr.getNodeName() + "=\"" +
                                               attr.getNodeValue() + "\"";
          }
          output = output + ">\n";
          txtResults.append(output);

        }

        //Print summary to console, opens the result window
        System.out.println(passed + " passed");
        System.out.println(failed + " failed");
        System.out.println(attempts + " attempts");
        frameResults.setVisible(true);
        if (failed != 0) {
         lblResult.setText("Number of Elements that appear in File 1, but not in File 2: " + failed);
        } else {
         lblResult.setText("All Elements that apear in File 1 appear in File 2");
        }
       }
      };

      //This line actually starts the SAX parser.
      saxParser.parse(lblFile1.getText(), handler);

      //Generic error handeling
     } catch (Exception eee) {
      JOptionPane.showMessageDialog(null, "An Error occured in the primary parser");
     }
    }
   }
  });
 }
}
