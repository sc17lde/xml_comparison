An XML comparison tool
Written by Liam Earle

This tool is designed to compare two XML files and will output the differences.

Compile the code with the command:
javac project_gui.java

Execute the code with the command:
java project_gui

This will open the programs GUI.

Click the "Select File 1" button and select the first XML file you want to compare.
The file name and address will be displayed as well as a short file preview.
This file preview box is limited to 100 lines so as to reduce memory usage.

Next, click the "Select File 2" button and select the second file.
This will be displayed in the same way as file 1.

Finally, click the "Compare Selected Files" once you have selected the correct files.
If no file has been selected, an error message will be displayed.

This will open a new window. This window will contain a list of all elements that
were found in the first file and not the second file, a divider and a second list of
all the elements that were found in the second file, but not the first file.

Though this program may be used as a full screen window, it is designed to be used as a
half window, beside a text editing software. The result page will also only display
elements missing in either file, not the surrounding context. This is intentional, as
displaying a few changes in a very large document is difficult to highlight.

The program also features a progress counter within the command line. This number
counts each element that has been processed. The comparison will complete once this
number reaches the number of elements in the first XML file. This is usually just
less than the line count.

######## Known Bugs ########

Text boxes may not display properly if the window is too short. Window width
does not affect this.
